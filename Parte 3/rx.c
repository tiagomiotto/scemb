#include <stdio.h>
#include <stdlib.h>
#include <cyg/io/io.h>
#include <cyg/kernel/kapi.h>
#include <string.h>
#include <ctype.h>

#include "serial.h"

// Mbox
extern cyg_handle_t mbx_serial_userH, regH;

// Serial
extern cyg_io_handle_t serH;

// Mutex
extern cyg_mutex_t scree_mtx, ring_mtx, processing_mtx;

extern cyg_alarm reg;
extern unsigned int mp;
extern unsigned int nr, ri, wi;

extern unsigned char *ring;
extern unsigned int lri, lnr;
extern bool fromProcessing;

extern cyg_sem_t needProcesing, util;



void read_until ( unsigned char *bufr);


void read_until ( unsigned char *bufr){
	int i=0;
	unsigned int n=1;
	unsigned char *aux;
	Cyg_ErrNo err;
	aux = (unsigned char*) calloc(1, sizeof(unsigned char));
	while (aux[0] != (unsigned char)EOM ){
		n = 1;
		//printf("estou preso\n");
		err = cyg_io_read(serH, aux, &n);
		//printf("io_read err=%x, n=%d\n", err, n);
		bufr[i] = aux[0];
		i++;
	}
	free(aux);
	n = i;
//	for (i=0; i<n; i++)
//		printf("rx[%d]=%x\n", i, bufr[i]);

}


void recive(cyg_addrword_t data){
	while(1){
		unsigned char bufr[50];
		read_until(bufr);
		if (bufr[1] == (unsigned char)NMFL ){
			mp = Mike;
			cyg_alarm_initialize(regH, cyg_current_time()+mp, mp);
			cyg_mutex_lock(&scree_mtx);
			printf("Memory half full!\n");
			cyg_mutex_unlock(&scree_mtx);
			cyg_mutex_lock(&processing_mtx);
			nr = bufr[3];
			ri = bufr[4];
			wi = bufr[5];
			cyg_mutex_unlock(&processing_mtx);
		}
		else if (bufr[1] == (unsigned char)IREG ){
			cyg_mutex_lock(&processing_mtx);
			nr = bufr[3];
			ri = bufr[4];
			wi = bufr[5];
			cyg_mutex_unlock(&processing_mtx);
			cyg_mbox_put( mbx_serial_userH, bufr );
		}
		else if ((bufr[1] == (unsigned char)TRGC || bufr[1] == (unsigned char)TRGI) && bufr[2] != (unsigned char)CMD_ERROR ){
			unsigned int i;
			cyg_mutex_lock(&ring_mtx);
			for (i=2;;i=i+5) {
				ring[lri++] = bufr[i];
				ring[lri++] = bufr[i+1];
				ring[lri++] = bufr[i+2]; 
				ring[lri++] = bufr[i+3]; 
				ring[lri++] = bufr[i+4];
				//printf("i: %d, i+1: %d, i+2: %d, i+3: %d, i+4: %d\n", bufr[i], bufr[i+1], bufr[i+2], bufr[i+3], bufr[i+4]);
				if (lnr < 500) {
					lnr++;
				}
				if( lri >= NRBUF*5){
					lri = 0;
				}
				if (bufr[i+5]==(unsigned char)EOM){
					break;
				}
			}
			cyg_semaphore_post (&needProcesing);

			cyg_mutex_unlock(&ring_mtx);
			if (fromProcessing) {
				fromProcessing = !fromProcessing;
			}
			else {
				cyg_mbox_put( mbx_serial_userH, bufr );
			}
		}
		else{
			cyg_mbox_put( mbx_serial_userH, bufr );
		}
		cyg_semaphore_post (&util);
	}
}