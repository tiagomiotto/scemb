#include <stdio.h>
#include <stdlib.h>
#include <cyg/io/io.h>
#include <cyg/kernel/kapi.h>
#include <string.h>
#include <ctype.h>

#include "serial.h"

extern cyg_handle_t mbx_user_serialH;


Cyg_ErrNo err;
cyg_io_handle_t serH;

void cmd_emh (int argc, unsigned char **argv);
void cmd_rmh (int argc, char **argv);
void read_msg ( unsigned char *bufr, unsigned int n );

void cmd_ini(int argc, char **argv);

void send (unsigned char msg[]);
void cmd_wait(void);
void initSerial (cyg_addrword_t data);



/*-------------------------------------------------------------------------+
| Function: cmd_emh - enviar mensagem (hexadecimal)
+--------------------------------------------------------------------------*/ 
void cmd_emh (int argc, unsigned char **argv)
{
	unsigned int n, i;
	unsigned char bufw[50];
	if ((n=argc) > 1) {
		for (i=0; i<n; i++){
			//unsigned int x; 
			//sscanf((char*)argv[i+1], "%x", &x); 
			//bufw[i]=(unsigned char)x;
			bufw[i] = *argv[i];
			//printf("bufw[%d]=%x\n", i, bufw[i]);
		}
		err = cyg_io_write(serH, bufw, &n);
//		printf("io_write err=%x, n=%d\n", err, n);
//		for (i=0; i<n; i++)
//			printf("buf[%d]=%x\n", i, bufw[i]);
//		printf("\n");
	}
	else {
		printf("nenhuma mensagem!!!\n");
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_rmh - receber mensagem (hexadecimal)
+--------------------------------------------------------------------------*/ 
void cmd_rmh (int argc, char **argv)
{
	unsigned int n, i;
	unsigned char bufr[50];

	if (argc > 1) n = atoi(argv[1]);
	if (n > 50) n = 50;
	err = cyg_io_read(serH, bufr, &n);
	printf("io_read err=%x, n=%d\n", err, n);
	for (i=0; i<n; i++)
		printf("buf[%d]=%x\n", i, bufr[i]);

}

void read_msg ( unsigned char *bufr, unsigned int n ){
	
	err = cyg_io_read(serH, bufr, &n);
	printf("io_read err=%x, n=%d\n", err, n);
//	int i;
//	for (i=0; i<n; i++)
//		printf("buf[%d]=%x\n", i, bufr[i]);
}


/*-------------------------------------------------------------------------+
| Function: cmd_ini - inicializar dispositivo
+--------------------------------------------------------------------------*/ 
void cmd_ini(int argc, char **argv)
{
	printf("io_lookup\n");
	if ((argc > 1) && (argv[1][0] = '1'))
		err = cyg_io_lookup("/dev/ser1", &serH);
	else err = cyg_io_lookup("/dev/ser0", &serH);
	printf("lookup err=%x\n", err);
}


/*-------------------------------------------------------------------------+
| Function: send - enviar mensagem
+--------------------------------------------------------------------------*/ 
void send ( unsigned char *msg){
	unsigned char **argv;
	int i = 1;
	
//	unsigned int k,j;
//	for (k=0; msg[k]!= (unsigned char)255; k++)
//			printf("argv[%d]=%x\n", k, msg[k]);
			
	argv =(unsigned char**) malloc(sizeof(unsigned char*)*50);
	argv[0] = (unsigned char*) malloc(sizeof(unsigned char));

	argv[0][0]= (unsigned char)SOM;
	while ( *(msg +(i-1)) != (unsigned char)255 ) {
		argv[i] = &msg[i-1];
		i++;
	}
	argv[i] = (unsigned char*) malloc(sizeof(unsigned char));
	argv[i][0] = (unsigned char)EOM;
	argv[i+1] = (unsigned char*) malloc(sizeof(unsigned char));
	argv[i+1][0] =(unsigned char)255;
//	for (j=0;*argv[j]!=(unsigned char)255;j++) {
//		printf("argv[%d]=%x\n", j, *argv[j]);
//	}
	cmd_emh(i+1, argv);
	free(argv[0]);
	free(argv[i]);
	free(argv[i+1]);
	free(argv);
}





/*-------------------------------------------------------------------------+
| Function: cmd_wait - wait for message to send
+--------------------------------------------------------------------------*/ 
void cmd_wait(void){
	unsigned char *msg;
	while(1){
		msg = cyg_mbox_get( mbx_user_serialH );
		send(msg);
	}	
}


void initSerial (cyg_addrword_t data){
	cmd_ini(0, NULL);
	cmd_wait();
}