/***************************************************************************
| File: comando.c  -  Concretizacao de comandos (exemplo)
|
| Autor: Carlos Almeida (IST)
| Data:  Maio 2008
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cyg/io/io.h>
#include <string.h>
#include <ctype.h>
#include <cyg/kernel/kapi.h>
#include <cyg/io/io.h>

#include "serial.h"

// Funcitions
extern void cmd_emh (int argc, char **argv);
extern void cmd_rmh (int argc, char **argv);
extern void getReg(cyg_handle_t alarmH, cyg_addrword_t data);

// Handles
extern cyg_handle_t mbx_user_serialH, mbx_serial_userH, regH, test_counterH, mbx_serial_processH;

// Alarm
extern cyg_alarm reg;

// Mutex
extern cyg_mutex_t scree_mtx, ring_mtx;

// Semaphor
extern cyg_sem_t needProcesing, util;
extern bool compute;

// Ring
extern unsigned char *ring;
extern unsigned int lri, lnr, iread, mp;
extern unsigned int ttl, ltl;
extern int cenas;

void cmd_sair (int argc, char **argv);
void cmd_rc (int argc, char **argv);
void cmd_sc (int argc, char **argv);
void cmd_rtl (int argc, char **argv);
void cmd_rp (int argc, char **argv);
void cmd_mmp (int argc, char **argv);
void cmd_mta (int argc, char **argv);
void cmd_ra (int argc, char **argv);
void cmd_dtl (int argc, char **argv);
void cmd_aa (int argc, char **argv);
void cmd_rmm (int argc, char **argv);
void cmd_cmm (int argc, char **argv);
void cmd_ir (int argc, char **argv);
void cmd_trc (int argc, char **argv);
void cmd_tri (int argc, char **argv);
void cmd_irl (int argc, char **argv);
void cmd_lr (int argc, char **argv);
void cmd_dr (int argc, char **argv);
void cmd_cpt (int argc, char **argv);
void cmd_mpt (int argc, char **argv);
void cmd_dttl (int argc, char **argv);
void cmd_pr (int argc, char **argv);


void cmd_sos (int argc, char **argv);
int my_getline (char** argv, int argvsize);





#define NCOMMANDS  (sizeof(commands)/sizeof(struct command_d))
#define ARGVECSIZE 10
#define MAX_LINE   50

/*-------------------------------------------------------------------------+
| Variable and constants definition
+--------------------------------------------------------------------------*/ 
const char TitleMsg[] = "\n Application Control Monitor\n";
const char InvalMsg[] = "\nInvalid command!";

struct 	command_d {
	void  (*cmd_fnct)(int, char**);
	char*	cmd_name;
	char*	cmd_help;
} 
const commands[] = {
	{cmd_emh,  "emh","<h1> <h2> <...>  enviar mensagem (hexadecimal)"},
	{cmd_rmh,  "rmh","<n>              receber mensagem (hexadecimal)"},
	{cmd_sos,  "sos","                 help"},
	{cmd_sair, "sair","                sair"},
	{cmd_rc,   "rc","                  read clock"},
	{cmd_sc,   "sc","h m s             set clock"},
	{cmd_rtl,  "rtl","                 read temperature and luminosity"},
	{cmd_rp,   "rp","                  read parameters (PMON, TALA)"},
	{cmd_mmp,  "mmp","p                modify monitoring period (seconds - 0 deactivate)"},
	{cmd_mta,  "mta","t                modify time alarm (seconds)"},
	{cmd_ra,   "ra","                  read alarms (temperature, luminosity, active/inactive-1/0)"},
	{cmd_dtl,  "dtl","t l              define alarm temerature and luminosity"},
	{cmd_aa,   "aa","a                 activate / deactivate alarma (1/0)"},
	{cmd_rmm,  "rmm","                 read maximums and minimums registers"},
	{cmd_cmm,  "cmm","                 clear maximums and minimums registers"},
	{cmd_ir,   "ir","                  information about registers (NREG, nr, iread, iwrite)"},
	{cmd_trc,  "trc","n                transfer n registers from current iread position"},
	{cmd_tri,  "tri","n i              transfer n registers from index i (0 - oldest)"},
	{cmd_irl,  "irl","                 information about local registers (NRBUF, nr, iread, iwrite)"},
	{cmd_lr,   "lr","n i               list n registers (local memory) from index i (0 - oldest)"},
	{cmd_dr,   "dr","                  delete registers (local memory)"},
	{cmd_cpt,  "cpt","                 check period of transference"},
	{cmd_mpt,  "mpt","p                modify period of transference (minutes - 0 deactivate)"},
	{cmd_dttl, "dttl","t l             define threshold temperature and luminosity for processing"},
	{cmd_pr,   "pr","[hi m1 s1 [h2 m2 s2]    process registers (max, min, mean) between instants t1 and t2 (h,m,s)"}
};


/*-------------------------------------------------------------------------+
| Function: cmd_sair - termina a aplicacao
+--------------------------------------------------------------------------*/ 
void cmd_sair (int argc, char **argv)
{
	exit(0);
}


/*-------------------------------------------------------------------------+
| Function: cmd_rc - Read clock
+--------------------------------------------------------------------------*/ 
void cmd_rc (int argc, char **argv){
	if (argc == 1 ) {
		// RCLK to comms thread
		// espera resposta
		unsigned char bufw[50];
		unsigned char *msg;
		bufw[0]=( unsigned char)RCLK;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("Time: %d:%d:%d\n", msg[2], msg[3], msg[4]);
		}
		cyg_mutex_unlock(&scree_mtx);
	}
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_sc - Set clock
+--------------------------------------------------------------------------*/ 
void cmd_sc (int argc, char **argv){
	unsigned int i,n;
	unsigned char bufw[50], *msg;
	if ((n=argc) == 4 ) {
		if (atoi(argv[1]) < 24 && atoi(argv[1]) >= 0 && atoi(argv[2]) < 60 && atoi(argv[3]) < 60 && atoi(argv[2]) >= 0 && atoi(argv[3]) >= 0) {
			// SCLK to comms thread
			// espera resposta
			bufw[0]=( unsigned char)SCLK;
			for (i=1; i<n; i++){
				char x[5];
				sprintf(x, "%c", atoi(argv[i]));
				bufw[i] = x[0]; 
			}
			bufw[i] = (unsigned char)255;
			
			cyg_mbox_put( mbx_user_serialH, bufw );
			
			msg = cyg_mbox_get( mbx_serial_userH );
			cyg_mutex_lock(&scree_mtx);
			if (msg[2] == (unsigned char)CMD_ERROR ){
				printf("Erro, tente de novo!");
			}
			else if ( msg[2] == (unsigned char)CMD_OK ){
				printf("Comando executado com sucesso.\n");
			}
			else{
				printf("Erro desconhecido!");
			}
			cyg_mutex_unlock(&scree_mtx);

					
		}
		else{
			cyg_mutex_lock(&scree_mtx);
			printf("Comando errado!!!\n");
			cyg_mutex_unlock(&scree_mtx);
			cyg_semaphore_post (&util);
		}
	}
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_rtl - Read temperature and luminosity
+--------------------------------------------------------------------------*/ 
void cmd_rtl (int argc, char **argv){
	if (argc == 1 ) {
		// RTL to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)RTL;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("Luminusidade: %d\t Temperatura: %d\n", msg[3], msg[2]);
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_rp - Read parameters (PMON, TALA)
+--------------------------------------------------------------------------*/ 
void cmd_rp (int argc, char **argv){
	if (argc == 1 ) {
		// RPAR to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)RPAR;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("PMON: %d\t TALA: %d\n", msg[2], msg[3]);
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_mmp - Modify monitoring period (seconds - 0 deactivate)
+--------------------------------------------------------------------------*/ 
void cmd_mmp (int argc, char **argv){
	unsigned int i,n;
	if ((n=argc) == 2 && atoi(argv[1]) >= 0 && atoi(argv[1]) <= 255) {
		// MMP to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)MMP;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0]; 
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else if ( msg[2] == (unsigned char)CMD_OK ){
			printf("Comando executado com sucesso.\n");
		}
		else{
			printf("Erro desconhecido!");
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_mta - modify time alarm (seconds)
+--------------------------------------------------------------------------*/ 
void cmd_mta (int argc, char **argv){
	unsigned int i,n;
	if ((n=argc) == 2 && atoi(argv[1]) >= 0 && atoi(argv[1]) <= 255) {
		// MTA to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)MTA;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0]; 
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else if ( msg[2] == (unsigned char)CMD_OK ){
			printf("Comando executado com sucesso.\n");
		}
		else{
			printf("Erro desconhecido!");
		}
		cyg_mutex_unlock(&scree_mtx);
	} 
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_ra - read alarms (temperature, luminosity, active/inactive-1/0)
+--------------------------------------------------------------------------*/ 
void cmd_ra (int argc, char **argv){
	if (argc == 1) {
		// RALA to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)RALA;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("Temperatura: %d\t Luminusidade: %d\t Alarms: %d\n", msg[2], msg[3], msg[4]);
		}
		cyg_mutex_unlock(&scree_mtx);
		
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_dtl - define alarm temerature and luminosity
+--------------------------------------------------------------------------*/ 
void cmd_dtl (int argc, char **argv){
	unsigned int i,n;
	if ((n=argc) == 3 && atoi(argv[1]) >= 0 && atoi(argv[2]) >= 0 && atoi(argv[1]) <= 50 && atoi(argv[2]) <= 3) {
		// DATL to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)DATL;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0]; 
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else if ( msg[2] == (unsigned char)CMD_OK ){
			printf("Comando executado com sucesso.\n");
		}
		else{
			printf("Erro desconhecido!");
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_aa - activate/deactivate alarms (1/0)
+--------------------------------------------------------------------------*/ 
void cmd_aa (int argc, char **argv){
	unsigned int i,n;
	unsigned char *msg;
	if ((n=argc) == 2 && (atoi(argv[1]) == 0 || atoi(argv[1]) == 1)) {
		// AALA to comms thread
		// espera resposta
		unsigned char bufw[50];
		bufw[0]=( unsigned char)AALA;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0];
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH ); 
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else if ( msg[2] == (unsigned char)CMD_OK ){
			printf("Comando executado com sucesso.\n");
		}
		else{
			printf("Erro desconhecido!");
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}


/*-------------------------------------------------------------------------+
| Function: cmd_rmm - read maximums and minimums registers
+--------------------------------------------------------------------------*/ 
void cmd_rmm (int argc, char **argv){
	if (argc == 1) {
		// RALA to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)RMM;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("Temperatura maxima:\n");
			printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n\n", msg[2], msg[3], msg[4], msg[5], msg[6]);
			
			printf("Temperatura minima:\n");
			printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n\n", msg[7], msg[8], msg[9], msg[10], msg[11]);
			
			printf("Luminusidade maxima:\n");
			printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n\n", msg[12], msg[13], msg[14], msg[15], msg[16]);
			
			printf("Luminusidade minima:\n");
			printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n\n", msg[17], msg[18], msg[19], msg[20], msg[21]);
			
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_cmm - clear maximums and minimus registers
+--------------------------------------------------------------------------*/ 
void cmd_cmm (int argc, char **argv){
	if (argc == 1 ) {
		// CMM to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)CMM;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else if ( msg[2] == (unsigned char)CMD_OK ){
			printf("Comando executado com sucesso.\n");
		}
		else{
			printf("Erro desconhecido!");
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_ir - information about registers (NREG, nr, iread, iwrite)
+--------------------------------------------------------------------------*/ 
void cmd_ir (int argc, char **argv){
	if (argc == 1 ) {
		// RTL to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)IREG;
		bufw[1] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw );
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
			printf("NREG: %d\t nr: %d\t iread: %d\t iwrite: %d\n", msg[2], msg[3], msg[4], msg[5]);		
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_trc - transfer n registers from current iread position
+--------------------------------------------------------------------------*/ 
void cmd_trc (int argc, char **argv){
	unsigned int i,n;
	if ((n=argc) == 2 && atoi(argv[1]) <= 20 ){
		// TRC to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)TRGC;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0]; 
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw ); 
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
//			for (i=2;;i=i+5) {
//				printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n", msg[i], msg[i+1], msg[i+2], msg[i+3], msg[i+4]);
//				if (msg[i+5]==(unsigned char)EOM){
//					break;
//				}
//			}
		}
		cyg_mutex_unlock(&scree_mtx);
		
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_tri - transfer n registers from index i (0 - oldest)
+--------------------------------------------------------------------------*/ 
void cmd_tri (int argc, char **argv){
	unsigned int i,n;
	if ((n=argc) == 3 && atoi(argv[1]) <= 20 && atoi(argv[2]) <= 20 ){
		// TRC to comms thread
		// espera resposta
		unsigned char bufw[50], *msg;
		bufw[0]=( unsigned char)TRGI;
		for (i=1; i<n; i++){
			char x[5];
			sprintf(x, "%c", atoi(argv[i]));
			bufw[i] = x[0]; 
		}
		bufw[i] = (unsigned char)255;
		cyg_mbox_put( mbx_user_serialH, bufw ); 
		
		msg = cyg_mbox_get( mbx_serial_userH );
		cyg_mutex_lock(&scree_mtx);
		if (msg[2] == (unsigned char)CMD_ERROR ){
			printf("Erro, tente de novo!");
		}
		else{
//			for (i=2;;i=i+5) {
//				printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n", msg[i], msg[i+1], msg[i+2], msg[i+3], msg[i+4]);
//				if (msg[i+5]==(unsigned char)EOM){
//					break;
//				}
//			}
			
		}
		cyg_mutex_unlock(&scree_mtx);
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
		cyg_semaphore_post (&util);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_irl - information about local registers (NRBUF, nr, iread, iwrite)
+--------------------------------------------------------------------------*/ 
void cmd_irl (int argc, char **argv){
	cyg_semaphore_post (&util);
	if (argc == 1){
		printf("NRBUF: %d\t nr: %d\t iread: %d\t iwrite: %d\n", NRBUF, lnr, iread ,lri);
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}
/*-------------------------------------------------------------------------+
| Function: cmd_lr - list n registers (local memory) from index i (0 - oldest)
+--------------------------------------------------------------------------*/ 
void cmd_lr (int argc, char **argv){
	cyg_semaphore_post (&util);
	if (argc == 3 && atoi(argv[1]) < NRBUF && atoi(argv[2]) < NRBUF && atoi(argv[2]) >= 0) {
		unsigned int i, j=0;
		int aux;
		if ((atoi(argv[1]) + atoi(argv[2])) < lnr) {
			printf("if\n");
			aux = atoi(argv[1]);
		}
		else{
			aux = lri/5 - atoi(argv[2]);
		}
		if (aux > 0){	
			cyg_mutex_lock(&ring_mtx);
			cyg_mutex_lock(&scree_mtx);
			for (i=(atoi(argv[2])*5); ;i=i+5 ) {
				printf("Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n", ring[i], ring[i+1], ring[i+2], ring[i+3], ring[i+4]);
				if ( (i + 5) > NRBUF*5) {
					i = 0;
				}
				j++;
				if (j >= aux) {
					break;
				}
				//cyg_thread_delay(200);
			}
			//iread = i+5;
			cyg_mutex_unlock(&scree_mtx);
			cyg_mutex_unlock(&ring_mtx);
		}
		else{
			cyg_mutex_lock(&scree_mtx);
			printf("Nao ha registos!\n");
			cyg_mutex_unlock(&scree_mtx);
		}
		
		
	}    
	else {
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_dr - delete registers (local memory)
+--------------------------------------------------------------------------*/ 
void cmd_dr (int argc, char **argv){
	cyg_semaphore_post (&util);
	if (argc == 1){
		unsigned int i;
		lri = 0;
		lnr = 0;
		iread = 0;
		for (i=0;i<NRBUF*5;i++) {
			ring[i]=0;
		}
		cyg_mutex_lock(&scree_mtx);
		printf("Registos locais apagados!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}


/*-------------------------------------------------------------------------+
| Function: cmd_cpt - check period of transference
+--------------------------------------------------------------------------*/ 
void cmd_cpt (int argc, char **argv){
	cyg_semaphore_post (&util);
	if (argc == 1){
		if (mp == 0) {
			cyg_mutex_lock(&scree_mtx);
			printf("Periodic of tranference disabled\n");
			cyg_mutex_unlock(&scree_mtx);
		}
		else{
			cyg_mutex_lock(&scree_mtx);
			printf("Period of tranference: %d minutes\n", mp/100/60);
			cyg_mutex_unlock(&scree_mtx);
		}
		
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_mpt - modify period of transference (minutes - 0 deactivate)
+--------------------------------------------------------------------------*/ 
void cmd_mpt (int argc, char **argv){
	cyg_semaphore_post (&util);
	if (argc == 2){
		if ( atoi(argv[1]) == 0 ) {
			cyg_alarm_disable(regH);
			cyg_mutex_lock(&scree_mtx);
			printf("Periodic tranference disabled\n");
			cyg_mutex_unlock(&scree_mtx);
		}
		else{
			mp = atoi(argv[1])*100*60;
			cyg_alarm_enable( regH );
			cyg_alarm_delete( regH );
			cyg_alarm_create(test_counterH, getReg, (cyg_addrword_t)0, &regH, &reg);
			cyg_alarm_initialize(regH, cyg_current_time()+mp, mp);
			cyg_mutex_lock(&scree_mtx);
			printf("Period of tranference is now: %d minutes\n", mp/100/60);
			cyg_mutex_unlock(&scree_mtx);
		}	
		
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_dttl - define threshold temperature and luminosity for processing
+--------------------------------------------------------------------------*/ 
void cmd_dttl (int argc, char **argv){
	cyg_semaphore_post (&util);
	if ( argc == 3  && atoi(argv[1]) >= 0 && atoi(argv[2]) >= 0 && atoi(argv[1]) <= 50 && atoi(argv[2]) <= 3) {
		ttl = atoi(argv[1]);
		ltl = atoi(argv[2]);
		cyg_mutex_lock(&scree_mtx);
		printf("\nProcessing Threshold: Temepratur: %d\t Luminosity: %d\n", ttl, ltl);
		cyg_mutex_unlock(&scree_mtx);
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}

/*-------------------------------------------------------------------------+
| Function: cmd_pr - process registers (max, min, mean) between instants t1 and t2 (h,m,s)
+--------------------------------------------------------------------------*/ 
void cmd_pr (int argc, char **argv){
	cyg_semaphore_post (&util);
	unsigned int n;
	if ( (n=argc) == 7 || argc == 4 || argc == 1 ) {
		unsigned char *msg;
		cenas = 255;
		if (argc == 4 || argc == 7) {
			cyg_mutex_lock(&scree_mtx);
			printf("A procura temporal nao foi implentada.\n");
			cyg_mutex_unlock(&scree_mtx);
			return;
		}
		compute = 1;
		cyg_semaphore_post (&needProcesing); 
		msg = cyg_mbox_get( mbx_serial_processH );
		cyg_mutex_lock(&scree_mtx);
		printf("TEMP: Max: %d\t min: %d\t mean: %d   LUM: Max: %d\t min: %d\t mean: %d\n", msg[0], msg[1], msg[2], msg[3], msg[4], msg[5]);
		cyg_mutex_unlock(&scree_mtx);
	}
	else{
		cyg_mutex_lock(&scree_mtx);
		printf("Comando errado!!!\n");
		cyg_mutex_unlock(&scree_mtx);
	}
}


/*-------------------------------------------------------------------------+
| Function: cmd_sos - provides a rudimentary help
+--------------------------------------------------------------------------*/ 
void cmd_sos (int argc, char **argv)
{
	cyg_semaphore_post (&util);
	int i;
	cyg_mutex_lock(&scree_mtx);
	printf("%s\n", TitleMsg);
	for (i=0; i<NCOMMANDS; i++)
		printf("%s %s\n", commands[i].cmd_name, commands[i].cmd_help);
	cyg_mutex_unlock(&scree_mtx);
}

/*-------------------------------------------------------------------------+
| Function: getline        (called from monitor) 
+--------------------------------------------------------------------------*/ 
int my_getline (char** argv, int argvsize)
{
	static char line[MAX_LINE];
	char *p;
	int argc;
	cyg_semaphore_wait (&util);
	fgets(line, MAX_LINE, stdin);

	/* Break command line into an o.s. like argument vector,
		 i.e. compliant with the (int argc, char **argv) specification --------*/

	for (argc=0,p=line; (*line != '\0') && (argc < argvsize); p=NULL,argc++) {
		p = strtok(p, " \t\n");
		argv[argc] = p;
		if (p == NULL) return argc;
	}
	argv[argc] = p;
	return argc;
}

/*-------------------------------------------------------------------------+
| Function: monitor        (called from main) 
+--------------------------------------------------------------------------*/ 
void monitor (cyg_addrword_t data)
{
	static char *argv[ARGVECSIZE+1], *p;
	int argc, i;
	cyg_mutex_lock(&scree_mtx);
	printf("%s Type sos for help\n", TitleMsg);
	cyg_mutex_unlock(&scree_mtx);
	for (;;) {
		cyg_mutex_lock(&scree_mtx);
		printf("\nCmd> ");
		cyg_mutex_unlock(&scree_mtx);
		/* Reading and parsing command line  ----------------------------------*/
		if ((argc = my_getline(argv, ARGVECSIZE)) > 0) {
			for (p=argv[0]; *p != '\0'; *p=tolower(*p), p++);
			for (i = 0; i < NCOMMANDS; i++){ 
	            if (strcmp(argv[0], commands[i].cmd_name) == 0) 
	                break;
			}
			/* Executing commands -----------------------------------------------*/
			if (i < NCOMMANDS)
	            commands[i].cmd_fnct (argc, argv);
			else{  
				cyg_mutex_lock(&scree_mtx);
				printf("%s", InvalMsg);
				cyg_mutex_unlock(&scree_mtx);
				cyg_semaphore_post (&util);
			}
		} /* if my_getline */
		else{
			cyg_semaphore_post (&util);
		}
	} /* forever */
}
