#include <stdio.h>
#include <stdlib.h>
#include <cyg/io/io.h>
#include <string.h>
#include <ctype.h>
#include <cyg/kernel/kapi.h>
#include <cyg/io/io.h>

#include "serial.h"

extern cyg_mutex_t processing_mtx, scree_mtx, ring_mtx;

// Mbox
extern cyg_handle_t mbx_user_serialH, mbx_serial_processH;

// Alarms
cyg_handle_t test_counterH, regH, system_clockH;
cyg_alarm reg;

// Periodo de tranferencia
unsigned int mp = 0;

unsigned int nr=0, ri=0, wi=0;
unsigned int ttl=30, ltl = 2;
bool compute = 0;
extern cyg_sem_t needProcesing;

// Ring buffer

extern unsigned char *ring;
unsigned int lri = 0, lnr = 0, iread = 0;
bool fromProcessing = 0;
int cenas;


void getReg(cyg_handle_t alarmH, cyg_addrword_t data){
	unsigned char bufw[50];
	cyg_mutex_lock(&processing_mtx);
	bufw[0]=( unsigned char)TRGC;
	if( ri > wi ){
		bufw[1] = (unsigned char)(nr-ri+wi);
	}
	else{
		bufw[1] = (unsigned char)(wi-ri);
	}
	bufw[2] = (unsigned char)255;
	cyg_mutex_unlock(&processing_mtx);
	fromProcessing = 1;
	cyg_mbox_put( mbx_user_serialH, bufw );
}

void initProcessing(cyg_addrword_t data){
	system_clockH = cyg_real_time_clock(); 
	cyg_clock_to_counter(system_clockH, &test_counterH);
	cyg_alarm_create(test_counterH, getReg, (cyg_addrword_t)0, &regH, &reg);
	unsigned int i;
	while (1) {
		cyg_semaphore_wait (&needProcesing); 
		if (compute == 1) {
			unsigned char bufw[50];
			int i, maxTemp=0, minTemp=255, meanTemp=0, maxLum=0, minLum=255, meanLum=0;
			cyg_mutex_lock(&ring_mtx);
			if ( cenas == 255 ) {
				for (i=0;i<(lnr*5);i=i+5) {
					if (maxTemp < ring[i+3]) {
						maxTemp = ring[i+3];
					}
					if (minTemp > ring[i+3]) {
						minTemp = ring[i+3];
					}
					meanTemp = meanTemp + ring[i+3];
					if (maxLum < ring[i+4]) {
						maxLum = ring[i+4];
					}
					if (minLum > ring[i+4]) {
						minLum = ring[i+4];
					}
					meanLum = meanLum + ring[i+4];
				}
				meanTemp = meanTemp/lnr;
				meanLum = meanLum/lnr;
			}
			else if ( cenas == 255 ) {
				
			}
			else {
				
			}
			cyg_mutex_unlock(&ring_mtx);
			bufw[0] = maxTemp; bufw[1] = minTemp; bufw[2] = meanTemp; bufw[3] = maxLum; bufw[4] = minLum; bufw[5]=meanLum;
			cyg_mbox_put( mbx_serial_processH, bufw );
			compute = 0;
		}
		else{
			cyg_mutex_lock(&ring_mtx);
			for (i=iread; i < lri ; i+=5) {
				//printf("Ring i+3: %d\t ttl: %d\t ring[i+4]: %d\t ltl: %d\t i: %d\n", ring[i+3], ttl, ring[i+4], ltl, i);
				if ( ring[i+3] > ttl || ring[i+4] > ltl ) {
					printf("Limit exceded: Time: %d:%d:%d\t Temperatura: %d\t Luminusidade: %d\n", ring[i], ring[i+1], ring[i+2], ring[i+3], ring[i+4]);
				}
			}
			iread = lri;
			cyg_mutex_unlock(&ring_mtx);
		}
	}
}

