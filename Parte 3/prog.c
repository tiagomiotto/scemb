#include <stdio.h>
#include <cyg/kernel/kapi.h>
#include <cyg/hal/hal_arch.h>

#include "serial.h"


extern void monitor(cyg_addrword_t data);
extern void initSerial(cyg_addrword_t data);
extern void recive(cyg_addrword_t data);
extern void initProcessing(cyg_addrword_t data);

#define STKSIZE 10000
#define NT 4
char stack[NT][STKSIZE];

// Mbox
cyg_handle_t mbx_user_serialH, mbx_serial_userH, mbx_serial_processH;
cyg_mbox mbx_user_serial, mbx_serial_user, mbx_serial_process;

// Threads
cyg_handle_t threadsH[NT];
cyg_thread   threads[NT];

// Mutex
cyg_mutex_t scree_mtx, processing_mtx, ring_mtx;

// Semaphor
cyg_sem_t needProcesing, util;

// Processing
unsigned char ring[NRBUF*5];


int main(void){
	unsigned int i = 0;
	for (i=0;i<NRBUF*5;i++) {
		ring[i] = 0;
	}
	// Mbox
	cyg_mbox_create( &mbx_user_serialH, &mbx_user_serial);
	cyg_mbox_create( &mbx_serial_userH, &mbx_serial_user);
	cyg_mbox_create( &mbx_serial_processH, &mbx_serial_process);
	
	// Mutex
	cyg_mutex_init(&scree_mtx);
	cyg_mutex_init(&processing_mtx);
	cyg_mutex_init(&processing_mtx);
	
	// Semaphors
	cyg_semaphore_init ( &needProcesing, (cyg_count32) 0);
	cyg_semaphore_init ( &util, (cyg_count32) 1);
	
//	printf("%d\n", cyg_clock_get_resolution(cyg_real_time_clock()));
//	while(1){
//		printf("%d\n",cyg_current_time());
//	}
	
	// Threads
	cyg_thread_create(9, initSerial, (cyg_addrword_t) 0,"TX Thread", (void *) stack[0], STKSIZE,&threadsH[0], &threads[0]);
	cyg_thread_create(5, recive, (cyg_addrword_t) 0,"RX Thread", (void *) stack[1], STKSIZE,&threadsH[1], &threads[1]);	
	cyg_thread_create(3, initProcessing, (cyg_addrword_t) 0,"Processing Thread", (void *) stack[2], STKSIZE,&threadsH[2], &threads[2]);
	
	
	cyg_thread_resume(threadsH[0]);
	cyg_thread_resume(threadsH[1]);
	cyg_thread_resume(threadsH[2]);
	
	
	monitor((cyg_addrword_t) 0);

	return 0;
}
