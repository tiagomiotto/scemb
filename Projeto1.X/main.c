/**
Generated Main Source File

Company:
Microchip Technology Inc.

File Name:
main.c

Summary:
This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

Description:
This header file provides implementations for driver APIs for all modules
selected in the GUI.
Generation Information :
Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
Device            :  PIC16F18875
Driver Version    :  2.00
 */

/*
(c) 2018 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software
and any
derivatives exclusively with Microchip products. It is your responsibility
to comply with third party
license terms applicable to your use of third party software (including open
source software) that
may accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
SOFTWARE.
 */

#include "i2c.h"
#include "mcc_generated_files/mcc.h"
long unsigned hour, min, sec;
int temp_alarm_time;
int index_eeprom = 0;
int valid_reg = 0;
int read_index = 0;

#define EEAddr 0x7000
#define TimeAddr 0x70AA
#define ConfigAddr 0x70B0
#define MaxAddr 0x70BA
/* It is assumed that SOM and EOM values do not occur in the message */
#define SOM 0xFD       /* start of message */
#define EOM 0xFE       /* end of message */
#define RCLK 0xC0      /* read clock */
#define SCLK 0XC1      /* set clock */
#define RTL 0XC2       /* read temperature and luminosity */
#define RPAR 0XC3      /* read parameters */
#define MMP 0XC4       /* modify monitoring period */
#define MTA 0XC5       /* modify time alarm */
#define RALA 0XC6      /* read alarms (temperature, luminosity, active/inactive) */
#define DATL 0XC7      /* define alarm temperature and luminosity */
#define AALA 0XC8      /* activate/deactivate alarms */
#define RMM 0XC9       /* read maximums and minimus registers */
#define CMM 0XCA       /* clear maximums and minimus registers */
#define IREG 0XCB      /* information about registers (NREG, nr, iread, iwrite)*/
#define TRGC 0XCC      /* transfer registers (curr. position)*/
#define TRGI 0XCD      /* transfer registers (index) */
#define NMFL 0XCE      /* notification memory (half) full */
#define CMD_OK 0       /* command successful */
#define CMD_ERROR 0xFF /* error in command */

int nreg = 20;
int pmon = 0;
int tala = 0;
int alat = 0;
int alaf = 0;
int alal = 0;
//Global Variables
unsigned char temperatura;
int auxi = 1;
int luminosity = 0;
bool clear_max = false;

/*
Main application
 */
void UART_Demo_Initialize(void)
{

    printf("\rPICDEM LAB II - Date 08/11/2015\r\n");
    printf("UART Communications 8-bit Rx and Tx\r\n\n");
    printf("Keyboard Type H : LED ON   Type L: LED OFF \r\n\n");
}

unsigned char tsttc(void)
{
    unsigned char value;
    do
    {
        IdleI2C();
        StartI2C();
        IdleI2C();

        WriteI2C(0x9a | 0x00);
        IdleI2C();
        WriteI2C(0x01);
        IdleI2C();
        RestartI2C();
        IdleI2C();
        WriteI2C(0x9a | 0x01);
        IdleI2C();
        value = ReadI2C();
        IdleI2C();
        NotAckI2C();
        IdleI2C();
        StopI2C();
    } while (!(value & 0x40));

    IdleI2C();
    StartI2C();
    IdleI2C();
    WriteI2C(0x9a | 0x00);
    IdleI2C();
    WriteI2C(0x00);
    IdleI2C();
    RestartI2C();
    IdleI2C();
    WriteI2C(0x9a | 0x01);
    IdleI2C();
    value = ReadI2C();
    IdleI2C();
    NotAckI2C();
    IdleI2C();
    StopI2C();

    return value;
}

void current_time();
void show_lum(float);
void save_to_eeprom_date();
void save_to_ring_buffer(int temperature, int luminosity);
void save_config();
void save_max_min(int temp_max, int temp_min, int lum_max, int lum_min);
void read_config();
void read_time();
void ledsSetLow(int num);
void user_interface();
void digitalWrite(int led, int value);
int setNumber(int end, int atual);
void serial_time(void);
bool read_registers(int n, int wanted_index, bool j);
void memory_half_full();

//Debouncer

bool S1_debounce()
{
    int pressed = 0;
    int contador = 0;
    do
    {
        if (PORTCbits.RC3 == 0)
        {                 //Se for a low zera o contador
            contador = 0; //Espera ficar 20ms a HIGH pra sair
            pressed = 1;
        }
        else
            contador++;
        __delay_ms(1);
    } while (contador < 20);

    if (pressed == 1)
        return true;
    else
        return false;
}

bool S2_debounce()
{
    int pressed = 0;
    int contador = 0;
    do
    {
        if (PORTCbits.RC4 == 0)
        {
            contador = 0;
            pressed = 1;
        }
        else
            contador++;
        __delay_ms(1);
    } while (contador < 20);

    if (pressed == 1)
        return true;
    else
        return false;
}

void main(void)
{

    int minutes_elapsed = min;
    // initialize the device
    SYSTEM_Initialize();
    TMR1_StartTimer();
    TMR2_StartTimer();

    INTERRUPT_PeripheralInterruptEnable();
    INTERRUPT_GlobalInterruptEnable();

    read_config();
    read_time();
    // When using interrupts, you need to set the Global and Peripheral Interrupt
    // Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    // INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    // INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    // INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    // INTERRUPT_PeripheralInterruptDisable();
    i2c1_driver_open();
    I2C_SCL = 1;
    I2C_SDA = 1;
    int last_measurement_time = counter;
    unsigned char last_temperatura;
    unsigned char max_temperatura = 0;
    unsigned char min_temperatura = (unsigned char)255;
    bool first = true;
    bool alarm_on = false;
    int last_luminosity = 0;
    int max_luminosity = 0;
    int min_luminosity = 3;
    // WPUC3 = 1;
    // WPUC4 = 1;
    //Start registers

    NOP();
    min_temperatura = tsttc();
    NOP();
    //min_luminosity = ADCC_GetSingleConversion(POT) >> 14;
//    save_max_min(0, 0, 0,
//                 min_luminosity);
    save_max_min(0, min_temperatura, 0,
                 255);

    while (1)
    {
        //        if (!EUSART_is_rx_ready()) {
        //            UART_Demo_Initialize();
        //
        //            auxi = EUSART_Read();
        //            printf("%x  auxi1\n", auxi);
        //            auxi = EUSART_Read();
        //            printf("%x  auxi2\n", auxi);
        //            auxi = EUSART_Read();
        //            printf("%x  auxi3\n", auxi);
        //
        //
        //        }
        current_time();
        // Measures every PMON TIME
        if (counter >= (last_measurement_time + pmon))
        {
            last_measurement_time = counter;
            last_temperatura = temperatura;
            NOP();
            temperatura = tsttc();
            NOP();
            last_luminosity = luminosity;
            luminosity = ADCC_GetSingleConversion(POT) >> 14;
        }

        // Saves max and min changes.
        if (luminosity >= max_luminosity)
        {
            max_luminosity = luminosity;
            save_max_min(0, 0, max_luminosity,
                         255);
        }
        if (luminosity < min_luminosity)
        {
            min_luminosity = luminosity;
            save_max_min(0, 0, 0,
                         min_luminosity);
        }
        if (temperatura > max_temperatura)
        {
            max_temperatura = temperatura;
            save_max_min(max_temperatura, 0, 0,
                         255);
        }
        if (temperatura < min_temperatura)
        {
            min_temperatura = temperatura;
            save_max_min(0, min_temperatura, 0,
                         255);
        }

        // Saves when value changes
        if (last_temperatura != temperatura || last_luminosity != luminosity)
        {
            save_to_ring_buffer((int)temperatura, luminosity);
            last_temperatura = temperatura;
            last_luminosity = luminosity;
        }
        // Saves every minute
        if (minutes_elapsed < min)
        {
            save_to_eeprom_date();
            minutes_elapsed = min;
        }
        // Show luminosity in leds D2 and D3
        show_lum(luminosity);

        // Alarm
        if (((int)temperatura > alat || luminosity > alal))
        {
            if (first)
            {
                temp_alarm_time = sec;
                first = false;
            }

            if (sec < (temp_alarm_time + tala))
            {
                PWM6_LoadDutyValue(300);
                alaf = 1;
            }
            else
            {
                PWM6_LoadDutyValue(1023);
            }
        }

        // User interface
        if (s1_p)
        {

            __delay_ms(30);
            s1_p = false;
            if (alaf == 1)
            { //IF alarm on
                first = true;
                PWM6_LoadDutyValue(0);
                alaf = 0;
            }
            else
            {
                user_interface();
            }
        }

        if (alaf == 0)
        {
            PIE4bits.TMR1IE = 1;

            NOP();
            //SLEEP();
            NOP();
        }
        serial_time();
        if (clear_max)
        {
            min_temperatura = (unsigned char)255;
            max_temperatura = 0;
            min_luminosity = 3;
            max_luminosity = 0;
            clear_max = false;
        }
        //UART_Demo_Initialize();
    }
}
//user_interface definition based on a Finite state machine deduced from the assignment

void user_interface()
{
#define noLed 0
#define LedD5 1
#define LedD4 2
#define LedD3 3
#define LedD2 4
#define configClock 1
#define configAlarm 2
#define configTemp 3
#define configLum 4
#define configHoursTens 5
#define configHoursUnits 6
#define configMinsTens 7
#define configMinsUnits 8
#define configAlarmValue 9
#define configTempTens 10
#define configTempUnits 11
#define configLumValue 12

    int config_state = configClock;
    int hourTens, hourUnits, minTens, minUnits, tempTens, tempUnits;
    ledsSetLow(1);
    while (1)
    {

        if (S1_GetValue() == 0)
        {
            s1_p = false;
            __delay_ms(300);

            switch (config_state)
            {
            case configClock:
                ledsSetLow(0);
                led_to_blink = LedD4;
                config_state = configAlarm;
                break;
            case configAlarm:
                ledsSetLow(0);
                led_to_blink = LedD3;
                config_state = configTemp;
                break;
            case configTemp:
                ledsSetLow(0);
                led_to_blink = LedD2;
                config_state = configLum;
                break;
            case configLum:
                ledsSetLow(0);
                led_to_blink = LedD5;
                save_config();
                s1_p = false;
                return; //Return aqui????
            case configHoursTens:
                config_state = configHoursUnits;
                break;
            case configHoursUnits:
                config_state = configMinsTens;
                break;
            case configMinsTens:
                config_state = configMinsUnits;
                break;
            case configMinsUnits:
                config_state = configAlarm;
                ledsSetLow(0);
                led_to_blink = LedD4;
                break;
            case configAlarmValue:
                config_state = configTemp;
                ledsSetLow(0);
                led_to_blink = LedD3;
                break;
            case configTempTens:
                config_state = configTempUnits;
                break;
            case configTempUnits:
                config_state = configLum;
                ledsSetLow(0);
                led_to_blink = LedD2;
                break;
            }
        }
        else if (S2_GetValue() == 0)
        {
            __delay_ms(300);
            //S2_debounce();
            switch (config_state)
            {
            case configClock:
                led_to_blink = noLed;
                config_state = configHoursTens;
                break;
            case configAlarm:
                led_to_blink = noLed;
                config_state = configAlarmValue;
                break;
            case configTemp:
                led_to_blink = noLed;
                config_state = configTempTens;
                break;
            case configLum:
                led_to_blink = noLed;
                config_state = configLumValue;
                break;
            }
        }
        int aux;
        switch (config_state)
        {
        case configHoursTens:
            ledsSetLow(0);
            aux = hour / 10;
            hourTens = setNumber(2, aux);
            config_state = configHoursUnits;
            break;
        case configHoursUnits:
            ledsSetLow(0);
            aux = hour % 10;
            if (hourTens == 2)
                hourUnits = setNumber(3, aux);
            else
                hourUnits = setNumber(9, aux);
            config_state = configMinsTens;
            break;
        case configMinsTens:
            ledsSetLow(0);
            aux = min / 10;
            minTens = setNumber(5, aux);
            config_state = configMinsUnits;
            break;
        case configMinsUnits:
            ledsSetLow(0);
            aux = min % 10;
            minUnits = setNumber(9, aux);
            int auxHora = hourTens * 10 + hourUnits;
            int auxMin = minTens * 10 + minUnits;
            counter = (auxHora * 3600) + (auxMin * 60) + sec;
            config_state = configAlarm;
            ledsSetLow(0);
            led_to_blink = LedD4;
            break;
        case configAlarmValue:
            ledsSetLow(0);
            alaf = setNumber(1, alaf);
            config_state = configTemp;
            ledsSetLow(0);
            led_to_blink = LedD3;
            break;
        case configTempTens:
            ledsSetLow(0);
            aux = alat / 10;
            tempTens = setNumber(5, aux);
            config_state = configTempUnits;
            break;
        case configTempUnits:
            ledsSetLow(0);
            aux = alat % 10;
            if (tempTens == 5)
                tempUnits = setNumber(0, aux);
            else
                tempUnits = setNumber(9, aux);
            alat = tempTens * 10 + tempUnits;
            config_state = configLum;
            ledsSetLow(0);
            led_to_blink = LedD2;
            break;
        case configLumValue:
            ledsSetLow(0);
            alal = setNumber(3, alal);
            ledsSetLow(0);
            led_to_blink = LedD5;
            save_config();
            return;
        }
    }
}

void ledsSetLow(int num)
{
    if (num == 0)
    {
        PWM6_LoadDutyValue(0);
        LED_D5_SetLow();
        LED_D3_SetLow();
        LED_D2_SetLow();
    }
    else
    {
        LED_D3_SetLow();
        LED_D2_SetLow();
        PWM6_LoadDutyValue(0);
    }
}
//Write value to leds

void digitalWrite(int led, int value)
{
    if (led == 5)
    {
        if (value == 1)
            LED_D5_SetHigh();
        else
            LED_D5_SetLow();
    }
    else if (led == 4)
    {
        if (value == 1)
            PWM6_LoadDutyValue(1023);
        else
            PWM6_LoadDutyValue(0);
    }
    else if (led == 3)
    {
        if (value == 1)
            LED_D3_SetHigh();
        else
            LED_D3_SetLow();
    }
    else if (led == 2)
    {
        if (value == 1)
            LED_D2_SetHigh();
        else
            LED_D2_SetLow();
    }
}

int setNumber(int end, int atual)
{ //Arrumar bounce
    ledsSetLow(0);
    digitalWrite(5, (atual & 0x8) >> 3);
    digitalWrite(4, (atual & 0x4) >> 2);
    digitalWrite(3, (atual & 0x2) >> 1);
    digitalWrite(2, (atual & 0x1));
    while (1)
    {

        if (S2_GetValue() == 0)
        {
            __delay_ms(300);
            // S2_debounce();
            ++atual;
        }
        if (atual > end)
        {
            atual = 0;
        }
        if (S1_GetValue() == 0)
        {
            __delay_ms(300);
            //S1_debounce();
            return atual;
        }
        digitalWrite(5, (atual & 0x8) >> 3);
        digitalWrite(4, (atual & 0x4) >> 2);
        digitalWrite(3, (atual & 0x2) >> 1);
        digitalWrite(2, (atual & 0x1));
    }
}

// Updates time

void current_time()
{
    hour = counter / 3600;
    min = (counter % 3600) / 60;
    sec = ((counter % 3600) % 60);
}
// Show lum in leds

void show_lum(float lum)
{
    if (((int)lum >> 1) == 1)
        LED_D3_SetHigh();
    else
        LED_D3_SetLow();

    if (((int)lum & 1) == 1)
        LED_D2_SetHigh();
    else
        LED_D2_SetLow();
}

// Saves date to the eprom

void save_to_eeprom_date()
{
    DATAEE_WriteByte(TimeAddr, hour);
    DATAEE_WriteByte(TimeAddr + 1, min);
    DATAEE_WriteByte(TimeAddr + 2, sec);
    DATAEE_WriteByte(TimeAddr + 3, (hour + min + sec));
}
// Saves data to ring buffer

void save_to_ring_buffer(int temperature, int luminosity)
{

    if (valid_reg < nreg)
        valid_reg++;

    DATAEE_WriteByte(EEAddr + (index_eeprom * 5), hour);
    DATAEE_WriteByte(EEAddr + (index_eeprom * 5) + 1, min);
    DATAEE_WriteByte(EEAddr + (index_eeprom * 5) + 2, sec);
    DATAEE_WriteByte(EEAddr + (index_eeprom * 5) + 3, temperature);
    DATAEE_WriteByte(EEAddr + (index_eeprom * 5) + 4, luminosity);
    if (valid_reg == (nreg / 2))
        memory_half_full();
    index_eeprom++;
        if (index_eeprom > nreg)
    {
        index_eeprom = 0;
    }
}

// Saves the current configuration

void save_config()
{

    DATAEE_WriteByte(ConfigAddr, pmon);
    DATAEE_WriteByte(ConfigAddr + 1, tala);
    DATAEE_WriteByte(ConfigAddr + 2, alat);
    DATAEE_WriteByte(ConfigAddr + 3, alal);
    DATAEE_WriteByte(ConfigAddr + 4, alaf);
    DATAEE_WriteByte(ConfigAddr + 5,
                     (pmon + tala + alat + alal + alaf)); // Checksum
}

// Save max and min values

void save_max_min(int temp_max, int temp_min, int lum_max, int lum_min)
{
    if (temp_max != 0)
    {
        DATAEE_WriteByte(MaxAddr + 1, hour);
        DATAEE_WriteByte(MaxAddr + 2, min);
        DATAEE_WriteByte(MaxAddr + 3, sec);
        DATAEE_WriteByte(MaxAddr + 4, temp_max);
        DATAEE_WriteByte(MaxAddr + 5, luminosity);
    }
    else if (temp_min != 0)
    {
        DATAEE_WriteByte(MaxAddr + 1 + 5, hour);
        DATAEE_WriteByte(MaxAddr + 2 + 5, min);
        DATAEE_WriteByte(MaxAddr + 3 + 5, sec);
        DATAEE_WriteByte(MaxAddr + 4 + 5, temp_min);
        DATAEE_WriteByte(MaxAddr + 5 + 5, luminosity);
    }
    else if (lum_max != 0)
    {
        DATAEE_WriteByte(MaxAddr + 1 + (5 * 2), hour);
        DATAEE_WriteByte(MaxAddr + 2 + (5 * 2), min);
        DATAEE_WriteByte(MaxAddr + 3 + (5 * 2), sec);
        DATAEE_WriteByte(MaxAddr + 4 + (5 * 2), temperatura);
        DATAEE_WriteByte(MaxAddr + 5 + (5 * 2), lum_max);
    }
    else if (lum_min != 255)
    {
        DATAEE_WriteByte(MaxAddr + 1 + (5 * 3), hour);
        DATAEE_WriteByte(MaxAddr + 2 + (5 * 3), min);
        DATAEE_WriteByte(MaxAddr + 3 + (5 * 3), sec);
        DATAEE_WriteByte(MaxAddr + 4 + (5 * 3), temperatura);
        DATAEE_WriteByte(MaxAddr + 5 + (5 * 3), lum_min);
    }
}

// Read the eeprom config at startup

void read_config()
{
    int aux_pmon = DATAEE_ReadByte(ConfigAddr + 0);
    int aux_tala = DATAEE_ReadByte(ConfigAddr + 1);
    int aux_alat = DATAEE_ReadByte(ConfigAddr + 2);
    int aux_alal = DATAEE_ReadByte(ConfigAddr + 3);
    int aux_alaf = DATAEE_ReadByte(ConfigAddr + 4);
    int checksum = DATAEE_ReadByte(ConfigAddr + 5);

    if (checksum == (aux_pmon + aux_tala + aux_alat + aux_alal + aux_alaf))
    {
        pmon = aux_pmon;
        tala = aux_tala;
        alat = aux_alat;
        alal = aux_alal;
        alaf = aux_alaf;
    }
    else
    {
        pmon = 3;
        tala = 2;
        alat = 30;
        alal = 2;
        alaf = 0;
    }
    save_config(); // Checksum
}
// Reads and updates time from eeprom

void read_time()
{
    int aux_hour = DATAEE_ReadByte(TimeAddr + 0);
    int aux_min = DATAEE_ReadByte(TimeAddr + 1);
    int aux_sec = DATAEE_ReadByte(TimeAddr + 2);
    int checksum = DATAEE_ReadByte(TimeAddr + 3);

    if (checksum == (aux_hour + aux_min + aux_sec))
    {
        counter = (aux_hour * 3600) + (aux_min * 60) + aux_sec;
    }
    else
        counter = 0;

    current_time();
    save_to_eeprom_date();
}

void serial_time(void)
{
    unsigned temp;
    uint8_t eom;
    int aux;
    if (EUSART_is_rx_ready())
    {
        //PIE3bits.RCIE = 0;
        temp = EUSART_Read();
        //printf(" %x \n", temperatura);
        if (temp == SOM)
        {
            temp = EUSART_Read();
            switch (temp)
            {
            case RCLK:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(RCLK);
                    EUSART_Write(hour);
                    EUSART_Write(min);
                    EUSART_Write(sec);
                    EUSART_Write(EOM);
                }
                break;
            case SCLK:
                temp = 0;
                unsigned int aux_h;
                unsigned int aux_m;
                aux_h = EUSART_Read();
                aux_m = EUSART_Read();
                temp = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    if (aux_h > 23 || aux_h < 0 ||
                        aux_m > 59 || aux_m < 0 || temp > 59 || temp < 0)
                    {
                        EUSART_Write(SOM);
                        EUSART_Write(SCLK);
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                        temp =0;
                        eom=0;
                    }
                    else
                    {
                        counter = (aux_h * 3600) + (aux_m * 60) + temp;
                        current_time();
                        save_to_eeprom_date();
                        EUSART_Write(SOM);
                        EUSART_Write(SCLK);
                        EUSART_Write(CMD_OK);
                        EUSART_Write(EOM);
                    }
                }else{
                                            EUSART_Write(SOM);
                        EUSART_Write(SCLK);
                        EUSART_Write(CMD_ERROR);
                        
                        EUSART_Write(EOM);
                        eom =0;
                        temp =0;
                }

                break;
            case RTL:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(RTL);
                    EUSART_Write(temperatura);
                    EUSART_Write(luminosity);
                    EUSART_Write(EOM);
                }
                break;
            case RPAR:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(RPAR);
                    EUSART_Write(pmon);
                    EUSART_Write(tala);
                    EUSART_Write(EOM);
                }
                break;
            case MMP:
                temp = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    if (temp >= 0 && temp < 100)
                    {
                        pmon = temp;
                        save_config();
                        EUSART_Write(SOM);
                        EUSART_Write(MMP);
                        EUSART_Write(CMD_OK);
                        EUSART_Write(EOM);
                    }
                    else
                    {
                        EUSART_Write(SOM);
                        EUSART_Write(MMP);
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                }

                break;
            case MTA:
                temp = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    if (temp >= 0 && temp < 61)
                    {
                        tala = temp;
                        save_config();
                        EUSART_Write(SOM);
                        EUSART_Write(MTA);
                        EUSART_Write(CMD_OK);
                        EUSART_Write(EOM);
                    }
                    else
                    {
                        EUSART_Write(SOM);
                        EUSART_Write(MTA);
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                }
                break;
            case RALA:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(RALA);
                    EUSART_Write(alat);
                    EUSART_Write(alal);
                    EUSART_Write(alaf);
                    EUSART_Write(EOM);
                }
                eom = 0;
                break;
            case DATL:
                temp = EUSART_Read();
                int alal_aux = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    if (temp < 0 || temp > 50 || alal_aux < 0 ||
                        alal_aux > 3)
                    {
                        EUSART_Write(SOM);
                        EUSART_Write(DATL);
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                    else
                    {
                        alat = temp;
                        alal = alal_aux;
                        save_config();
                        EUSART_Write(SOM);
                        EUSART_Write(DATL);
                        EUSART_Write(CMD_OK);
                        EUSART_Write(EOM);
                    }
                }
                break;
            case AALA:
                temp = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    if (temp < 0)
                    {
                        EUSART_Write(SOM);
                        EUSART_Write(AALA);
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                    else
                    {
                        if (temp > 0)
                            alaf = 1;
                        else
                            alaf = 0;
                        save_config();
                        EUSART_Write(SOM);
                        EUSART_Write(AALA);
                        EUSART_Write(CMD_OK);
                        EUSART_Write(EOM);
                    }
                }
                break;
            case RMM:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(RMM);
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 1 + (5 * 0)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 2 + (5 * 0)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 3 + (5 * 0)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 4 + (5 * 0)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 5 + (5 * 0)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 1 + (5 * 1)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 2 + (5 * 1)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 3 + (5 * 1)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 4 + (5 * 1)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 5 + (5 * 1)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 1 + (5 * 2)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 2 + (5 * 2)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 3 + (5 * 2)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 4 + (5 * 2)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 5 + (5 * 2)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 1 + (5 * 3)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 2 + (5 * 3)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 3 + (5 * 3)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 4 + (5 * 3)));
                    EUSART_Write(DATAEE_ReadByte(MaxAddr + 5 + (5 * 3)));
                    EUSART_Write(EOM);
                }
                break;
            case CMM:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    DATAEE_WriteByte(MaxAddr + 1, 0);
                    DATAEE_WriteByte(MaxAddr + 2, 0);
                    DATAEE_WriteByte(MaxAddr + 3, 0);
                    DATAEE_WriteByte(MaxAddr + 4, 0);
                    DATAEE_WriteByte(MaxAddr + 5, 0);

                    DATAEE_WriteByte(MaxAddr + 1 + 5, 0);
                    DATAEE_WriteByte(MaxAddr + 2 + 5, 0);
                    DATAEE_WriteByte(MaxAddr + 3 + 5, 0);
                    DATAEE_WriteByte(MaxAddr + 4 + 5, 0);
                    DATAEE_WriteByte(MaxAddr + 5 + 5, 0);

                    DATAEE_WriteByte(MaxAddr + 1 + (5 * 2), 0);
                    DATAEE_WriteByte(MaxAddr + 2 + (5 * 2), 0);
                    DATAEE_WriteByte(MaxAddr + 3 + (5 * 2), 0);
                    DATAEE_WriteByte(MaxAddr + 4 + (5 * 2), 0);
                    DATAEE_WriteByte(MaxAddr + 5 + (5 * 2), 0);

                    DATAEE_WriteByte(MaxAddr + 1 + (5 * 3), 0);
                    DATAEE_WriteByte(MaxAddr + 2 + (5 * 3), 0);
                    DATAEE_WriteByte(MaxAddr + 3 + (5 * 3), 0);
                    DATAEE_WriteByte(MaxAddr + 4 + (5 * 3), 0);
                    DATAEE_WriteByte(MaxAddr + 5 + (5 * 3), 0);
                    clear_max = true;
                    EUSART_Write(SOM);
                    EUSART_Write(CMM);
                    EUSART_Write(CMD_OK);
                    EUSART_Write(EOM);
                }
                break;
            case IREG:
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(IREG);
                    EUSART_Write(nreg);
                    EUSART_Write(valid_reg);
                    EUSART_Write(read_index);
                    EUSART_Write(index_eeprom);
                    EUSART_Write(EOM);
                }
                break;
            case TRGC:
                temp = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(TRGC);
                    if (read_registers(temp, 0, false))
                        EUSART_Write(EOM);
                    else
                    {
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                }
                break;
            case TRGI:
                temp = EUSART_Read();
                int auxi = EUSART_Read();
                eom = EUSART_Read();
                if (eom == EOM)
                {
                    EUSART_Write(SOM);
                    EUSART_Write(TRGI);
                    if (read_registers(temp, auxi, true))
                        EUSART_Write(EOM);
                    else
                    {
                        EUSART_Write(CMD_ERROR);
                        EUSART_Write(EOM);
                    }
                }
                break;
            case NMFL:
                break;
            case 1:
                break;
            }
            //temp[0] = 0;
            //temp[1] = 0;
            //temp[2] = 0;
        }
    }
    //if(EUSART_is_tx_done()) EUSART_Write(0x31);
    //PIE3bits.RCIE = 1;
}

bool read_registers(int n, int wanted_index, bool j)
{
    if ( (valid_reg < 20) &&
            ((n + read_index) > valid_reg || (n + wanted_index) > valid_reg))
        return false;
    int i;
    if (!j)
    {
        int k=0;
        int aux = n;

        for (i = read_index;; i++)
        {
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5)));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 1));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 2));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 3));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 4));
            if(i>=nreg) i=0;
            k++;
            if(k>=aux) break;
           
        }
        read_index += n;
    }
    else
    {
        int k=0;
        int aux = n;
        for (i = wanted_index;; i++)
        {
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5)));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 1));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 2));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 3));
            EUSART_Write(DATAEE_ReadByte(EEAddr + (i * 5) + 4));
            if(i>=nreg) i=0;
            k++;
            if(k>=aux) break;
        }
    }

    if (read_index >= nreg)
        read_index = 0;
}

void memory_half_full()
{
    EUSART_Write(SOM);
    EUSART_Write(NMFL);
                        EUSART_Write(nreg);
                    EUSART_Write(valid_reg);
                    EUSART_Write(read_index);
                    EUSART_Write(index_eeprom);
    EUSART_Write(EOM);
}
/**
End of File
 */
